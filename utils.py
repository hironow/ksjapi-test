# -*- coding: utf-8 -*-
# import requests
# from lxml import etree
import urllib
import urllib2


def get_status():
    url = ('http://nlftp.mlit.go.jp/ksj/api/1.0b/index.php'
           '/app/getKSJSummary.xml')
    params = {
        'appId': 'ksjapibeta1',
        'lang': 'J',
        'dataformat': 1
    }

    url_params = urllib.urlencode(params)
    full_url = url + '?' + url_params
    r = urllib2.urlopen(full_url)

    return r.read()
