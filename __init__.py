# -*- coding: utf-8 -*-
"""
/***************************************************************************
 KSJAPI
                                 A QGIS plugin
 KSJAPI Test
                             -------------------
        begin                : 2015-08-01
        copyright            : (C) 2015 by hironow
        email                : nishiyama@goga.co.jp
        git sha              : $Format:%H$
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
 This script initializes the plugin, making it known to QGIS.
"""


# noinspection PyPep8Naming
def classFactory(iface):  # pylint: disable=invalid-name
    """Load KSJAPI class from file KSJAPI.

    :param iface: A QGIS interface instance.
    :type iface: QgsInterface
    """
    #
    from PyQt4.QtCore import QTextCodec
    QTextCodec.setCodecForCStrings(QTextCodec.codecForName('UTF-8'))

    from .ksjapi import KSJAPI
    return KSJAPI(iface)
